const prefixo = "#viewCodigoMorse";
const codigo = "TRANSMISSAO DE DADOS";
let arrTimeouts = [];

export function carregarCodigoMorse() {
    const morseText = textToMorse(codigo);
    const tempoCodigoMorse = 42000; //total de tempo para reproduzir o código morse acima

    for (let index = 0; index < 10; index++) {
        let idTimeout = setTimeout(
            () => {
                playMorseCode(morseText);
            },
            index === 1000 ? 0 : tempoCodigoMorse * index
        );
        arrTimeouts.push(idTimeout);
    }

    $(`${prefixo} #divPortaEletronica`).click(() =>
        $(`${prefixo} #divPortaEletronica #inputCodigo`).focus()
    );

    $(`${prefixo} #divPortaEletronica #inputCodigo`).keyup(function () {
        if (`${this.value}`.toUpperCase() == codigo) {
            $(`${prefixo} #divPortaEletronica #btnUnlockDoor`).removeClass(
                "btn-outline-dark"
            );
            $(`${prefixo} #divPortaEletronica #btnUnlockDoor`).addClass(
                "btn-success"
            );
            $(`${prefixo} #divPortaEletronica #btnUnlockDoor`).prop(
                "disabled",
                false
            );

            blinkButton($(`${prefixo} #divPortaEletronica #btnUnlockDoor`));

            $(`${prefixo} #divPortaEletronica #btnUnlockDoor`).click(() => {
                arrTimeouts.forEach(idTimeout => {
                    clearTimeout(idTimeout);
                });
                playBeep = () => {};
                playMorseCode = () => {};
                proximaFase();
            });
        }
    });
}

let playBeep = time => {
    const audioContext = new (window.AudioContext ||
        window.webkitAudioContext)();

    const oscillator = audioContext.createOscillator();
    oscillator.connect(audioContext.destination);
    oscillator.type = "sine";

    // Set the frequency of the beep (e.g., 440 Hz for an A4 note)
    oscillator.frequency.setValueAtTime(440, audioContext.currentTime);

    oscillator.start();
    oscillator.stop(audioContext.currentTime + time);
};

// Function to play Morse code
let playMorseCode = morseText => {
    const outputDiv = document.getElementById("outputCodigoMorse");
    const lightDiv = document.getElementById("light");
    const morseArray = morseText.split("");
    let index = 0;

    function playNextSymbol() {
        outputDiv.innerHTML = morseText
            .slice(0, index + 1)
            .replaceAll("/", "<br>");
        if (index < morseArray.length) {
            if (morseArray[index] === ".") {
                lightDiv.style.backgroundColor = "#99ff8b";
                playBeep(0.2);
                setTimeout(() => {
                    lightDiv.style.backgroundColor = "#353535";
                    index++;
                    setTimeout(playNextSymbol, 200); // Dot duration
                }, 200); // Gap between dots and dashes
            } else if (morseArray[index] === "-") {
                lightDiv.style.backgroundColor = "#99ff8b";
                playBeep(0.6);
                setTimeout(() => {
                    lightDiv.style.backgroundColor = "#353535";
                    index++;
                    setTimeout(playNextSymbol, 600); // Dash duration
                }, 600);
            } else if (morseArray[index] === " " || morseArray[index] === "/") {
                setTimeout(() => {
                    index++;
                    playNextSymbol();
                }, 400); // Gap between words
            }
        } else {
        }
    }

    index = 0;
    playNextSymbol();
};

// Function to convert text to Morse code
function textToMorse(text) {
    return text
        .toUpperCase()
        .split("")
        .map(char => {
            if (morseCode[char]) {
                return morseCode[char];
            } else {
                return "";
            }
        })
        .join(" ");
}

// Morse code mapping
const morseCode = {
    A: ".-",
    B: "-...",
    C: "-.-.",
    D: "-..",
    E: ".",
    F: "..-.",
    G: "--.",
    H: "....",
    I: "..",
    J: ".---",
    K: "-.-",
    L: ".-..",
    M: "--",
    N: "-.",
    O: "---",
    P: ".--.",
    Q: "--.-",
    R: ".-.",
    S: "...",
    T: "-",
    U: "..-",
    V: "...-",
    W: ".--",
    X: "-..-",
    Y: "-.--",
    Z: "--..",
    1: ".----",
    2: "..---",
    3: "...--",
    4: "....-",
    5: ".....",
    6: "-....",
    7: "--...",
    8: "---..",
    9: "----.",
    0: "-----",
    " ": "/",
};
