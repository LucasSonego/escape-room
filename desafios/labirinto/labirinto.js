const Y = 0;
const X = 1;

const prefixo = "#viewLabirinto";

let comandos = [];
let emExecucao = false;
let cartaoColetado = false;

let labirinto = [];

export function carregarLabirinto() {
    carregarFuncoesBotoesComandos();
    labirintoLimpo();
    exibirLabirinto(labirinto, () => {
        showPopover($(`${prefixo} #containerLabirinto .robo`), "Ola!", 4000);
    });

    $(`#btnExitViewLabirinto`).click(() => {
        $(`${prefixo} #containerLabirinto .robo`).popover("hide");
        proximaFase();
    });

    $(`${prefixo} button`).click(() =>
        $(`${prefixo} #containerLabirinto .robo`).popover("hide")
    );
}

function labirintoLimpo() {
    labirinto = templateLabirinto.map(linha =>
        linha.map(casa => casa.map(conteudo => conteudo))
    );
}

function exibirLabirinto(casas, callback = () => {}) {
    $(`${prefixo} #containerLabirinto`).empty();
    casas.forEach((row, rowIndex) => {
        $(`${prefixo} #containerLabirinto`).append(
            `<div id="row${rowIndex}" class="linha">`
        );
        row.forEach((casa, casaIndex) => {
            $(`${prefixo} #row${rowIndex}`).append(htmlCasa(casa, casaIndex));
        });
    });

    callback();
}

function carregarFuncoesBotoesComandos() {
    $(`${prefixo} #btnComandoUp`).click(() => {
        comandos.push(new Comando("up", 1));
        exibirComandos();
    });
    $(`${prefixo} #btnComandoDown`).click(() => {
        comandos.push(new Comando("down", 1));
        exibirComandos();
    });
    $(`${prefixo} #btnComandoLeft`).click(() => {
        comandos.push(new Comando("left", 1));
        exibirComandos();
    });
    $(`${prefixo} #btnComandoRight`).click(() => {
        comandos.push(new Comando("right", 1));
        exibirComandos();
    });

    $(`${prefixo} #btnExecutar`).click(() => {
        if (comandos.length > 0 && !emExecucao) {
            emExecucao = true;

            $(`${prefixo} #btnExecutar`).html(
                `Parar <i class="fas fa-play"></i>`
            );

            executarComandos();
            return;
        }

        if (emExecucao) {
            $(`${prefixo} #btnExecutar`).html(
                `Executar <i class="fas fa-play"></i>`
            );
            emExecucao = false;

            return;
        }
    });
}

function exibirComandos() {
    $(`${prefixo} #comandosAdicionados`).empty();

    comandos.forEach((comando, index) => {
        $(`${prefixo} #comandosAdicionados`).append(
            htmlComando(comando, index)
        );

        $(`${prefixo} #comando${index} #btnMinus${index}`).click(() => {
            if (emExecucao) return;

            if (comando.quantidade > 1) comando.quantidade--;

            $(`${prefixo} #comando${index} #inputQtd${index}`).val(
                comando.quantidade
            );

            $(`${prefixo} #comando${index} .descricao`).text(
                comando.getDesricao()
            );
        });

        $(`${prefixo} #comando${index} #btnPlus${index}`).click(() => {
            if (emExecucao) return;

            if (comando.quantidade < 99) comando.quantidade++;

            $(`${prefixo} #comando${index} #inputQtd${index}`).val(
                comando.quantidade
            );

            $(`${prefixo} #comando${index} .descricao`).text(
                comando.getDesricao()
            );
        });

        $(`${prefixo} #comando${index} #inputQtd${index}`).keyup(function () {
            if (emExecucao) {
                this.value = comando.quantidade;
                return;
            }

            this.value = this.value.replace(/[^0-9]/g, "");
            if (Number(this.value) > 99) this.value = 99;
            else comando.quantidade = Number(this.value);

            $(`${prefixo} #comando${index} .descricao`).text(
                comando.getDesricao()
            );
        });

        $(`${prefixo} #comando${index} #inputQtd${index}`).blur(function () {
            if (this.value == "") {
                this.value = 1;
                comando.quantidade = 1;
                $(`${prefixo} #comando${index} .descricao`).text(
                    comando.getDesricao()
                );
            }
        });

        $(`${prefixo} #btnRemoverComando${index}`).click(() => {
            removerComando(index);
        });
    });
}

function removerComando(index) {
    comandos.splice(index, 1);
    exibirComandos();
}

function executarComandos(indexComando = 0) {
    moverRobo(comandos[indexComando])
        .then(sucesso => {
            if (sucesso) {
                if (indexComando + 1 < comandos.length) {
                    executarComandos(indexComando + 1);
                } else {
                    labirintoLimpo();

                    let msg = "";

                    emExecucao = false;
                    $(`${prefixo} #btnExecutar`).html(
                        `Executar <i class="fas fa-play"></i>`
                    );

                    if (cartaoColetado) {
                        //remover cartão da exibição
                        labirinto[labirinto.length - 1] = labirinto[
                            labirinto.length - 1
                        ].map(casa =>
                            casa.filter(conteudo => conteudo != "cartao")
                        );
                        msg = "Consegui pegar o cartão, aqui está ele!";

                        $(`${prefixo} #btnExecutar`).prop("disabled", true);
                        $(`#btnExitViewLabirinto`).slideDown(200);
                        blinkButton($(`#btnExitViewLabirinto`), 5);
                    } else {
                        msg = "Não encontrei nada, vamos tentar novamente?";
                    }

                    setTimeout(() => {
                        exibirLabirinto(labirinto, () =>
                            showPopover(
                                $(`${prefixo} #containerLabirinto .robo`),
                                msg
                            )
                        );
                    }, 1000);
                }
            }
        })
        .catch(erro => {
            let msg = "";
            if (erro) {
                emExecucao = false;
                $(`${prefixo} #btnExecutar`).html(
                    `Executar <i class="fas fa-play"></i>`
                );

                msg =
                    "Ops, tem algo de errado na programação, dei de cara na parede :/";
            }

            labirintoLimpo();
            exibirLabirinto(labirinto, () =>
                showPopover($(`${prefixo} #containerLabirinto .robo`), msg)
            );
        });
}

/**
 * @param {Comando} comando
 */
function moverRobo(comando, tempo = 500) {
    return new Promise((resolve, reject) => {
        let posicaoRobo = getPosicaoRobo(labirinto);

        for (
            let movimentos = 1;
            movimentos <= comando.quantidade;
            movimentos++
        ) {
            setTimeout(() => {
                if (!emExecucao) {
                    reject(false);
                    return;
                }

                if (comando.tipo == "up") {
                    if (
                        labirinto[posicaoRobo[Y] - 1][posicaoRobo[X]].includes(
                            "wall"
                        ) ||
                        posicaoRobo[Y] - 1 < 0
                    ) {
                        reject(true);
                        return;
                    }

                    labirinto[posicaoRobo[Y]][posicaoRobo[X]] = labirinto[
                        posicaoRobo[Y]
                    ][posicaoRobo[X]].filter(conteudo => conteudo != "robo");

                    labirinto[posicaoRobo[Y] - 1][posicaoRobo[X]].push("robo");

                    posicaoRobo[Y]--;
                } else if (comando.tipo == "down") {
                    if (
                        labirinto[posicaoRobo[Y] + 1][posicaoRobo[X]].includes(
                            "wall"
                        ) ||
                        posicaoRobo[Y] + 1 >= labirinto.length
                    ) {
                        reject(true);
                        return;
                    }

                    labirinto[posicaoRobo[Y]][posicaoRobo[X]] = labirinto[
                        posicaoRobo[Y]
                    ][posicaoRobo[X]].filter(conteudo => conteudo != "robo");

                    labirinto[posicaoRobo[Y] + 1][posicaoRobo[X]].push("robo");

                    posicaoRobo[Y]++;

                    if (
                        labirinto[posicaoRobo[Y]][posicaoRobo[X]].includes(
                            "cartao"
                        )
                    ) {
                        cartaoColetado = true;
                        resolve(true);
                        return;
                    }
                } else if (comando.tipo == "left") {
                    if (
                        labirinto[posicaoRobo[Y]][posicaoRobo[X] - 1].includes(
                            "wall"
                        ) ||
                        posicaoRobo[X] - 1 < 0
                    ) {
                        reject(true);
                        return;
                    }

                    labirinto[posicaoRobo[Y]][posicaoRobo[X]] = labirinto[
                        posicaoRobo[Y]
                    ][posicaoRobo[X]].filter(conteudo => conteudo != "robo");

                    labirinto[posicaoRobo[Y]][posicaoRobo[X] - 1].push("robo");

                    posicaoRobo[X]--;
                } else if (comando.tipo == "right") {
                    if (
                        labirinto[posicaoRobo[Y]][posicaoRobo[X] + 1].includes(
                            "wall"
                        ) ||
                        posicaoRobo[X] + 1 >= labirinto[0].length
                    ) {
                        reject(true);
                        return;
                    }

                    labirinto[posicaoRobo[Y]][posicaoRobo[X]] = labirinto[
                        posicaoRobo[Y]
                    ][posicaoRobo[X]].filter(conteudo => conteudo != "robo");

                    labirinto[posicaoRobo[Y]][posicaoRobo[X] + 1].push("robo");

                    posicaoRobo[X]++;
                }

                exibirLabirinto(labirinto);
            }, tempo * movimentos);
        }

        setTimeout(() => {
            resolve(true);
        }, tempo * comando.quantidade);
    });
}

function getPosicaoRobo(labirintoAtual) {
    let posicaoRobo = [];
    labirintoAtual.forEach((row, rowIndex) => {
        row.forEach((casa, casaIndex) => {
            if (casa.includes("robo")) {
                posicaoRobo = [rowIndex, casaIndex];
            }
        });
    });
    return posicaoRobo;
}

class Comando {
    constructor(tipo, quantidade) {
        this.tipo = tipo;
        this.quantidade = quantidade;
    }

    getDesricao() {
        switch (this.tipo) {
            case "up":
                return `casa${this.quantidade > 1 ? "s" : ""} para cima`;
            case "down":
                return `casa${this.quantidade > 1 ? "s" : ""} para baixo`;
            case "left":
                return `casa${this.quantidade > 1 ? "s" : ""} para a esquerda`;
            case "right":
                return `casa${this.quantidade > 1 ? "s" : ""} para a direita`;
        }
    }
}

const htmlCasa = (casa, index) =>
    `<div  
        id="casa${index}" 
        class="casa 
        ${casa.includes("wall") ? "parede" : ""}
        ${casa.includes("exit") ? "saida" : ""}
        "
    >
        ${
            casa.includes("robo")
                ? `<img class="robo" src="/assets/img/robo.svg"/>`
                : casa.includes("cartao")
                ? `<img class="cartao" src="/assets/img/cartao-sm.svg"/>`
                : !casa.includes("wall")
                ? `<i class="fas fa-circle"></i>`
                : ""
        }
    </div>`;

const htmlComando = (comando, index) =>
    `<div class="comando d-flex justify-content-between align-items-center" id="comando${index}">
        <div>
            <i class="fas fa-arrow-${comando.tipo} me-2"></i>
            <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                <button id="btnMinus${index}" type="button" class="btn btn-outline-secondary btn-sm">-</button>
                <input 
                    class="text-center" 
                    id="inputQtd${index}" 
                    value="${comando.quantidade}"
                >
                <button id="btnPlus${index}" type="button" class="btn btn-outline-secondary btn-sm">+</button>
            </div>
            <span class="descricao">${comando.getDesricao()}</span>
        </div>
        <i class="fas fa-times" id="btnRemoverComando${index}"></i>
    </div>
    `;

const templateLabirinto = [
    [
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["door", "entrance", "robo"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
    ],
    [
        ["wall"],
        [],
        ["wall"],
        [],
        [],
        [],
        [],
        [],
        ["wall"],
        [],
        [],
        [],
        [],
        [],
        ["wall"],
    ],
    [
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        [],
        ["wall"],
    ],
    [
        ["wall"],
        [],
        [],
        [],
        ["wall"],
        [],
        [],
        [],
        ["wall"],
        [],
        [],
        [],
        ["wall"],
        [],
        ["wall"],
    ],
    [
        ["wall"],
        [],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        [],
        ["wall"],
        ["wall"],
        [],
        ["wall"],
        ["wall"],
        ["wall"],
        [],
        ["wall"],
    ],
    [
        ["wall"],
        [],
        [],
        [],
        ["wall"],
        [],
        [],
        [],
        ["wall"],
        [],
        ["wall"],
        [],
        [],
        [],
        ["wall"],
    ],
    [
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
    ],
    [
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        [],
        [],
        [],
        [],
        [],
        ["wall"],
        [],
        ["wall"],
    ],
    [
        ["wall"],
        ["wall"],
        ["wall"],
        [],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
    ],
    [
        ["wall"],
        [],
        [],
        [],
        ["wall"],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        ["wall"],
    ],
    [
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
    ],
    [
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        [],
        [],
        [],
        ["wall"],
        [],
        ["wall"],
    ],
    [
        ["wall"],
        [],
        ["wall"],
        ["wall"],
        ["wall"],
        [],
        ["wall"],
        [],
        ["wall"],
        ["wall"],
        ["wall"],
        [],
        ["wall"],
        ["wall"],
        ["wall"],
    ],
    [
        ["wall"],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        ["wall"],
        [],
        [],
        [],
        [],
        [],
        ["wall"],
    ],
    [
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["door", "exit", "cartao"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
        ["wall"],
    ],
];
