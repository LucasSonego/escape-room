const prefixo = "#viewCifraCesar";

let idFase = 0;
const codigos = ["CRIPTOGRAFIA", "APRENDIZAGEM"];

export function carregarCifraCesar() {
    carregarEventosPortaEletronica();
    carregarEventosRodaCifraCesar();
}

function carregarEventosPortaEletronica() {
    $(`${prefixo} #btnUnlockDoor`).click(evnet => {
        evnet.stopPropagation();
    });

    $(`${prefixo} #divPortaEletronica #inputCodigoPorta`).on(
        "keyup",
        function () {
            if (`${this.value}`.toUpperCase() == codigos[idFase]) {
                $(`${prefixo} #divPortaEletronica #btnUnlockDoor`).removeClass(
                    "btn-outline-dark"
                );
                $(`${prefixo} #divPortaEletronica #btnUnlockDoor`).addClass(
                    "btn-success"
                );
                $(`${prefixo} #divPortaEletronica #btnUnlockDoor`).prop(
                    "disabled",
                    false
                );

                blinkButton($(`${prefixo} #divPortaEletronica #btnUnlockDoor`));

                $(`${prefixo} #divPortaEletronica #btnUnlockDoor`).click(() =>
                    handleUnlockDoor()
                );
            }
        }
    );
}

function handleUnlockDoor() {
    if (idFase == 0) {
        idFase++;
        limparEventos(`${prefixo} #divPortaEletronica #btnUnlockDoor`);

        $(`${prefixo}`).hide();

        $(`${prefixo} #divPortaEletronica #btnUnlockDoor`).removeClass(
            "btn-success"
        );

        $(`${prefixo} #divPortaEletronica #btnUnlockDoor`).addClass(
            "btn-outline-dark"
        );

        $(`${prefixo} #divPortaEletronica #btnUnlockDoor`).prop(
            "disabled",
            true
        );

        $(`${prefixo} #divPortaEletronica #inputCodigoPorta`).val("");

        $(`${prefixo} #divPortaEletronica`).css(
            "background-image",
            `url("/assets/img/porta-eletronica-2.svg")`
        );

        $("#containerTextos").show();

        exibirParagrafos([
            "Na porta de saída da sala seguinte encontram um código maior.",
            "Cada numero representa uma das letras do código do cartão.",
        ]).then(() => {
            $("#containerTextos").hide();
            $(`${prefixo}`).show();
        });
    } else {
        proximaFase();
    }
}

function carregarEventosRodaCifraCesar() {
    $(`${prefixo} #divPortaEletronica`).click(() => {
        $(`${prefixo} #divPortaEletronica #inputCodigoPorta`).focus();
    });

    $(`${prefixo} #sliderRodaCifraCesar`).on("input", function () {
        $(`${prefixo} #spanValorSlider`).text(
            `${this.value > 0 ? "+" : ""}${this.value}`
        );

        $(`${prefixo} #imgCifraCesarCentro`).css(
            "transform",
            `rotate(${(this.value * 28 * 180) / 360}deg)`
        );
    });
}
