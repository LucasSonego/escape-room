const prefixo = "#viewBinario";
const codigo = "2583";

export function carregarBinario() {
    $(`${prefixo} #divPortaEletronica`).click(() => {
        $(`${prefixo} #inputCodigo`).focus();
    });

    $(`${prefixo} #inputCodigo`).keyup(function () {
        if (`${this.value}`.toUpperCase() == codigo) {
            $(`${prefixo} #btnUnlockDoor`).removeClass("btn-outline-dark");
            $(`${prefixo} #btnUnlockDoor`).addClass("btn-success");
            $(`${prefixo} #btnUnlockDoor`).prop("disabled", false);

            blinkButton($(`${prefixo} #btnUnlockDoor`));

            $(`${prefixo} #btnUnlockDoor`).click(() => {
                $(`${prefixo}`).hide();
                proximaFase();
            });
        }
    });
}
