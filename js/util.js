function limparEventos(seletor) {
    const element = document.querySelector(seletor);
    const clonedElement = element.cloneNode(true);
    element.replaceWith(clonedElement);
    return clonedElement;
}

function intervaloAleatorio(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function showPopover(element, content, time = 10000) {
    element.popover({
        content: content,
        trigger: "manual",
        offset: "0, 20",
    });
    element.popover("show");
    setTimeout(() => {
        element.popover("hide");
    }, time);
}

function blinkButton(button, blinks = 3, blinkTime = 800) {
    let classBtn = button.attr("class");
    //filter string from the first occurrence of "btn-"" to the next space
    let tipo = classBtn.substring(
        classBtn.indexOf("btn-") + 4,
        classBtn.length
    );
    tipo = tipo.substring(
        0,
        tipo.indexOf(" ") != -1 ? tipo.indexOf(" ") : tipo.length
    );
    tipo = tipo.replace("-", "_");

    for (let i = 0; i < blinks; i++) {
        setTimeout(() => {
            cssFocusBts[tipo]?.forEach(css => {
                button.css(css.split(":")[0], css.split(":")[1]);
            });
        }, blinkTime * i);
        setTimeout(() => {
            cssFocusBts[tipo]?.forEach(css => {
                button.css(css.split(":")[0], "");
            });
        }, blinkTime * i + blinkTime / 2);
    }
}

let cssFocusBts = {
    outline_info: ["box-shadow: 0 0 0 0.25rem rgba(13,202,240,.5)"],
    success: [
        "background-color: #157347",
        "border-color: #146c43",
        "box-shadow: 0 0 0 0.25rem rgba(60,153,110,.5)",
    ],
    outline_success: ["box-shadow: 0 0 0 0.25rem rgba(25,135,84,.5)"],
};
