let indexSalaAtual = 0;
const tempoDica = 60000; // 1 minuto
let arrTimeoutsDicas = [];

$(document).ready(() => {
    carregarSala("introducao");

    $("#btnDicas").click(() => $("#modalDicas").modal("show"));
    $("#modalDicas .fa-times").click(() => $("#modalDicas").modal("hide"));
});

function carregarSala(identificador) {
    indexSalaAtual = listaSalas.findIndex(
        sala => sala.identificador == identificador
    );
    if (indexSalaAtual == -1) return;

    $("#salaAtualCabecalho span").text(listaSalas[indexSalaAtual].nome);

    exibirListaSalas(listaSalas, listaSalas[indexSalaAtual].identificador);
    $("#containerDesafio").hide();
    $("#containerTextos").show();

    arrTimeoutsDicas.forEach(timeout => clearTimeout(timeout));
    arrTimeoutsDicas = [];
    $("#btnDicas").hide();
    $("#listaDicas").empty();

    exibirHeadlines(listaSalas[indexSalaAtual].titulos).then(() =>
        exibirParagrafos(listaSalas[indexSalaAtual].textos).then(() => {
            if (listaSalas[indexSalaAtual].desafio) {
                $("#containerTextos").hide();
                $("#containerDesafio").show();
                carregarDesafio(listaSalas[indexSalaAtual].desafio);

                if (listaSalas[indexSalaAtual].dicas) {
                    for (
                        let indexDica = 0;
                        indexDica < listaSalas[indexSalaAtual].dicas.length;
                        indexDica++
                    ) {
                        let timeoutDica = setTimeout(() => {
                            if (indexDica == 0) $("#btnDicas").show();

                            $("#listaDicas").append(
                                `<li>${listaSalas[indexSalaAtual].dicas[indexDica]}</li>`
                            );

                            blinkButton($("#btnDicas"), 5);
                        }, tempoDica * (indexDica + 1));

                        arrTimeoutsDicas.push(timeoutDica);
                    }
                }
            } else {
                proximaFase();
            }
        })
    );
}

function carregarDesafio(desafio) {
    $("#containerDesafio").load(desafio);
}

function proximaFase() {
    indexSalaAtual++;

    if (listaSalas[indexSalaAtual]) {
        carregarSala(listaSalas[indexSalaAtual].identificador);
    } else {
        arrTimeoutsDicas.forEach(timeout => clearTimeout(timeout));
        arrTimeoutsDicas = [];

        $("#containerTextos").show();
        exibirHeadlines(textoFinal.titulos).then(() =>
            exibirParagrafos(textoFinal.textos).then(() => {
                $("#containerTextos").append(
                    `<button id="btnReiniciar" class="btn btn-light"><i class="fas fa-sync me-2"></i>Reiniciar</button>`
                );

                $("#btnReiniciar").click(() => {
                    $("#btnReiniciar").remove();
                    carregarSala(listaSalas[0].identificador);
                });
            })
        );
    }
}
