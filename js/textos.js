/**
 * Exibe lista de headlines
 * @param {string[]} headlines
 * @param {number} index
 * @returns {Promise}
 */
function exibirHeadlines(headlines) {
    $("#containerTextos #headlines").empty();
    $("#containerTextos #btnContinuar").css("visibility", "hidden");

    return new Promise(resolve => {
        headlines.forEach((headline, index) => {
            setTimeout(() => {
                $("#containerTextos #headlines").append(`<h3>${headline}</h3>`);
            }, index * 1000);
        });
        setTimeout(() => {
            limparEventos("#containerTextos #btnContinuar");
            $("#containerTextos #btnContinuar").click(() => resolve());
            $("#containerTextos #btnContinuar").css("visibility", "visible");
        }, headlines.length * 1000);
    });
}

/**
 * Percorre array de textos e exibe um por vez conforme clique no botão continuar
 * @param {string[]} textos
 * @param {*} index
 * @returns {Promise}
 */
function exibirParagrafos(textos, index = 0) {
    $("#containerTextos #btnContinuar").css("visibility", "hidden");

    return new Promise(resolve => {
        textoDigitado(textos[index], "#containerTextos #paragrafo").then(() => {
            limparEventos("#containerTextos #btnContinuar");

            if (index < textos.length - 1) {
                //se ainda não é o último texto
                $("#containerTextos #btnContinuar").click(() => {
                    $("#containerTextos #btnContinuar").css(
                        "visibility",
                        "hidden"
                    );
                    apagarTexto("#containerTextos #paragrafo", 500).then(() =>
                        exibirParagrafos(textos, index + 1).then(() =>
                            resolve()
                        )
                    );
                });
                $("#containerTextos #btnContinuar").css(
                    "visibility",
                    "visible"
                );
            } else {
                //se for o último texto
                $("#containerTextos #btnContinuar").click(() => {
                    $("#containerTextos #btnContinuar").css(
                        "visibility",
                        "hidden"
                    );
                    apagarTexto("#containerTextos #paragrafo", 500).then(() => {
                        resolve();
                    });
                });
                $("#containerTextos #btnContinuar").css(
                    "visibility",
                    "visible"
                );
            }
        });
    });
}

/**
 * Exibir texto com animação de digitação
 * @param {string} texto
 * @param {string} elemento seletor
 * @returns {Promise}
 */
function textoDigitado(texto, elemento) {
    return new Promise(resolve => {
        let arrCaracteres = texto.trim().split("");
        let intervaloAgregado = 0;
        $(elemento).empty();

        arrCaracteres.forEach(caractere => {
            intervaloAgregado += intervaloAleatorio(10, 40);
            // intervaloAgregado += intervaloAleatorio(1, 5);
            setTimeout(() => {
                $(elemento).append(caractere);
            }, intervaloAgregado);
        });

        setTimeout(() => {
            resolve();
        }, intervaloAgregado);
    });
}

/**
 * Animação de apagar texto
 * @param {string} elemento - seletor
 * @param {number} tempo ms
 * @returns {Promise}
 */
function apagarTexto(elemento, tempo) {
    return new Promise(resolve => {
        let texto = $(elemento).text();
        let arrCaracteres = texto.trim().split("");
        let intervaloAgregado = 0;

        arrCaracteres.forEach((_, index) => {
            intervaloAgregado += tempo / arrCaracteres.length;
            setTimeout(() => {
                $(elemento).text(texto.substring(0, texto.length - index - 1));
            }, intervaloAgregado);
        });

        setTimeout(() => {
            resolve();
        }, tempo);
    });
}
