function exibirListaSalas(salas, salaAtual) {
    $(`#listaSalas`).empty();

    salas.forEach((sala, index) => {
        $(`#listaSalas`).append(
            htmlItemSala(sala, sala.identificador == salaAtual, index)
        );
        $(`#listaSalas #itemSala${index}`).click(() => console.log(sala));
    });
}

const htmlItemSala = (sala, salaAtual, index) =>
    `<li id="itemSala${index}" ${salaAtual ? 'class="sala-atual"' : ""}>
    ${sala.nome}
  </li>`;
