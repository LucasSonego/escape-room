let listaSalas = [
    {
        identificador: "introducao",
        nome: "Introdução",
        titulos: ["Uma visita ao laboratório", "Que se tornou uma aventura"],
        textos: [
            "Um grupo de alunos estava visitando um laboratório de desenvolvimento \
            de tecnologias digitais, e já haviam passado por diversas áreas.",
            "Enquanto visitavam o setor de desenvolvimento de robôs, ouviram um \
            barulho alto e as luzes se apagaram por alguns segundos.",
            "Quando as luzes se acenderam novamente, perceberam que a professora \
            e o guia já não estavam mais junto deles.",
            "Após esperarem alguns minutos e ninguém aparecer, decidiram tentar \
            encontrar uma forma de sair, mas todas as portas deste laboratório só \
            podiam ser abertas com cartões de acesso ou com senhas.",
        ],
    },
    {
        identificador: "robotica_labirinto",
        titulos: ["Sala de Robótica", "O Labirinto"],
        nome: "Robótica - O Labirinto",
        textos: [
            "Na sala de robótica, procurando por uma forma de sair, os alunos avistaram um cartão, \
            que poderia ser usado para abrir as portas trancadas.",
            "Mas não conseguiam chegar até ele, pois precisavam passar por caminhos muito apertados,\
            que eram utilizados para testar os robôs.",
            "Ao lado estava um pequeno robô, que antes estava em testes, então tiveram a ideia de \
            tentar programar o robô para passar pelos corredores estreitos e trazer o cartão.",
        ],
        desafio: "desafios/labirinto/labirinto.html",
    },
    {
        identificador: "seguranca_criptografia",
        titulos: ["Sala de Segurança de Dados", "Os Códigos Criptografados"],
        nome: "Segurança de dados - Códigos criptografados",
        textos: [
            "A porta que conseguiram abrir dava acesso a sala de segurança de dados, os alunos \
            se dirigem à porta de saída.",
            "Tentaram abrir a porta com o mesmo cartão obtido na sala anterior, \
            mas a porta não abre, o leitor de cartão está danificado.",
            "Mas existe uma segunda forma de abrir a porta, \
            junto do leitor existe um teclado onde pode ser inserida uma senha. ",
            `Na parte de trás do cartão existe um código, e na tela da \
            fechadura da porta está escrito “+12”`,
            `Para descobrir o código que abre a porta, os alunos precisam \
            decifrar o código do cartão com essa chave "+12"`,
        ],
        desafio: "desafios/cifra-cesar/cifra-cesar.html",
        dicas: [
            "Use a barra acima do disco para girar as letras conforme a chave que aparece na tela da fechadura.",
            "Lembre-se que a forma de decifrar com o disco é procurando as letras do cartão no disco de fora, \
            e no disco de dentro está a letra que você quer descobrir.",
            "Se a chave for apenas um numero, use-o para todas as letras, se forem vários, \
            use cada um para uma letra diferente.",
        ],
    },
    {
        identificador: "redes_codigo_morse",
        titulos: ["Sala de Redes e Telecomunicações", "Um Som Estranho"],
        nome: "Redes e Telecomunicações - Um som estranho",
        textos: [
            "Os alunos chegaram à sala onde se estudava as redes de internet e de telecomunicações.",
            `Ao entrar na sala, os alunos notaram um som estranho, "tu tuuuuu tu…".\
            Uma luz na fechadura ficava piscando, sincronizada com o som que ouviam.`,
            "A fechadura da porta de saída era diferente das que haviam visto nas outras portas.",
            "Nela havia uma sequência de símbolos “.” e “-” com alguns espaços entre eles.\
            A luz e o som eram sincronizados, e seguiam o mesmo padrão visto nos símbolos escritos na porta.",
            "O ponto representava um som e uma piscada rápida e o traço durava um pouco mais.",
        ],
        desafio: "desafios/codigo-morse/codigo-morse.html",
        dicas: ["As quebras de linha representam espaços entre as letras."],
    },
    {
        identificador: "armazenamento_janelas",
        titulos: ["Sala de Armazenamento de Dados", "As janelas e a Saída"],
        nome: "Armazenamento de dados - As janelas",
        textos: [
            "Os alunos entraram na sala onde se estudava armazenamento de dados. \
            Nesta sala encontram uma porta que finalmente parecia ser a saída.",
            "Ao lado da porta haviam diversas janelas pequenas, a maioria fechadas, \
            mas algumas estavam abertas, e por elas era possível ver a luz do sol. ",
            "A porta possuía apenas um teclado numérico, mas não há mais nenhum \
            indício de algo que pareça ser uma pista para a senha.",
            "Um dos alunos notou um padrão nas janelas, talvez possam ser os números \
            que abrem a porta, mas em binário.",
        ],
        desafio: "desafios/binario/binario.html",
        dicas: [
            "Cada janela representa um bit, se a janela estiver aberta, o bit é 1, se estiver fechada, o bit é 0.",
            "Os bits são lidos da direita para a esquerda, o bit mais a direita é o bit menos significativo.",
            "Cada linha de janelas representa um dos números que abrem a porta.",
            "Exemplo: Se as janelas forem 0 1 1 0, o calculo fica 0x8 + 1x4 + 1x2 + 0x1 = 6.",
        ],
    },
];

let textoFinal = {
    titulos: ["A Saída", "O Fim de Uma Aventura"],
    textos: [
        "Trabalhando em equipe, os alunos conseguiram sair do laboratório.",
        "E também aprenderam um pouco sobre os temas estudados em cada sala.",
    ],
};
